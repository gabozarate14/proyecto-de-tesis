import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import axios from 'axios';


const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: theme.palette.background.paper,
    // marginTop: theme.spacing(8),
    padding: theme.spacing(6, 0),
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  section1: {
    margin: theme.spacing(3, 2),
  },
  section2: {
    margin: theme.spacing(2),
  },
}));

export default function UploadFileArticles(props) {
  const classes = useStyles();
  const { description, title } = props;
  const [file, setFile] = useState(null);
  const [filename, setFilename] = useState('Choose File');

  const onChange = async e => {
    console.log(e.target.files[0].name)
    setFile(e.target.files[0]);
    setFilename(e.target.files[0].name);
    console.log(filename);
  };

  const onSubmit = async e => {
    e.preventDefault();

    console.log(file);
    console.log(filename);

    const formData = new FormData();
    formData.append('file', file);
    formData.append('name', filename);
    (axios.post('http://localhost:5000/predict_articles', formData))
      .then(res => {
        console.log(res);
        console.log(res.data);
      });
  }

  return (
    <div className={classes.form}>
      <div className={classes.section1}>
        <label htmlFor="upload-file">
          <input

            id="upload-file"
            name="upload-file"
            type="file"
            onChange={onChange}
          />

        </label>
        <Divider variant="middle" />
      </div>
      <div className={classes.section2}>
        <Button color="secondary" variant="contained" component="span" onClick={onSubmit}>
          Cargar Archivo
     </Button>
      </div>
    </div>
  );
}

