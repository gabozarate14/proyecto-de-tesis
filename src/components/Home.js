import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import GitHubIcon from '@material-ui/icons/GitHub';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import MainFeaturedPost from './Header';
import TweetsCard from './TweetsCard';
import Main from './Main';
import Sidebar from './Sidebar';
import Footer from './Footer';
import ArticleCard from './ArticleCard';


const useStyles = makeStyles((theme) => ({
  mainGrid: {
    marginTop: theme.spacing(3),
  },
}));


const mainFeaturedPost = {
  title: 'Analítica de datos en información pública de medios periodísticos y redes sociales para el análisis de sentimiento',
  description:
    "La siguiente web servirá para automatizar la definición de sentimiento de notas periodísticas y estandarizar los criterios usados por los expertos al momento de realizar un análisis sentimental",
  image: 'https://www.exponencialconfirming.com/wp-content/uploads/2019/03/blog_inteligencia_artificial-min.jpg',
  imgText: 'main image description',
  linkText: 'Más información…',
};

const cards = [
  {
    title: 'Artículos Periodísticos',
    date: 'Medios de comunicación diversos',
    description:
      'Subir un archivo para realizar un análisis de sentimiento en un conjunto de artículos periodísticos',
    image: 'https://images.unsplash.com/photo-1588681664899-f142ff2dc9b1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80',
    imageText: 'Image Text',
  },
  {
    title: 'Publicaciones de Redes Sociales',
    date: 'Facebook, Twitter, etc.',
    description:
    'Subir un archivo para realizar un análisis de sentimiento en un conjunto de publicaciones de redes sociales',
    image: 'https://images.unsplash.com/photo-1579869847557-1f67382cc158?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1491&q=80',
    imageText: 'Image Text',
  },
];

const posts = [];

const sidebar = {
  title: 'About',
  description:
    'Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.',
  archives: [
    { title: 'March 2020', url: '#' },
    { title: 'February 2020', url: '#' },
    { title: 'January 2020', url: '#' },
    { title: 'November 1999', url: '#' },
    { title: 'October 1999', url: '#' },
    { title: 'September 1999', url: '#' },
    { title: 'August 1999', url: '#' },
    { title: 'July 1999', url: '#' },
    { title: 'June 1999', url: '#' },
    { title: 'May 1999', url: '#' },
    { title: 'April 1999', url: '#' },
  ],
  social: [
    { name: 'GitHub', icon: GitHubIcon },
    { name: 'Twitter', icon: TwitterIcon },
    { name: 'Facebook', icon: FacebookIcon },
  ],
};

export default function Home() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg">
        
        <main>
          <MainFeaturedPost post={mainFeaturedPost} />
          <Grid container spacing={4}>
              <ArticleCard key={cards[0].title} post={cards[0]} />
              <TweetsCard key={cards[1].title} post={cards[1]} />
          </Grid>
          <Grid container spacing={5} className={classes.mainGrid}>
            <Main title="From the firehose" posts={posts} />
            <Sidebar
              title={sidebar.title}
              description={sidebar.description}
              archives={sidebar.archives}
              social={sidebar.social}
            />
          </Grid>
        </main>
      </Container>
      <Footer title="Proyecto de Tesis" description="PONTIFICIA UNIVERSIDAD CATÓLICA DEL PERÚ" />
    </React.Fragment>
  );
}